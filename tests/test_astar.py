def test_a_star():
    from src.graph import dijkstra
    from src.graph import a_star

    def h(n1: int, n2: int) -> float:
        weights = {
            1: {1: 0, 2: 1, 3: 1, 4: 2, 5: 2, 6: 1},
            2: {1: 1, 2: 0, 3: 1, 4: 1, 5: 2, 6: 2},
            3: {1: 1, 2: 1, 3: 0, 4: 1, 5: 2, 6: 1},
            4: {1: 2, 2: 1, 3: 1, 4: 0, 5: 1, 6: 2},
            5: {1: 2, 2: 2, 3: 2, 4: 1, 5: 0, 6: 1},
            6: {1: 1, 2: 2, 3: 1, 4: 2, 5: 1, 6: 0},
        }
        return weights[n1][n2]

    n = 6
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            d_actual = dijkstra(GRAPH, i, j)
            a_actual = a_star(GRAPH, i, j, h)
            assert d_actual == a_actual, f"A* should give the same results as Dijkstra for path between {i} and {j}, expected {d_actual}, found {a_actual}"
