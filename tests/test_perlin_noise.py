from perlin_noise import PerlinNoise


def test_terrain():
    from src.project import generate_terrain
    expected = [
        ['g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g'],
        ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w'],
        ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w'],
        ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w'],
        ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w'],
        ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w'],
        ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w'],
        ['w', 'w', 'w', 'w', 'w', 'g', 'g', 'g', 'g', 'w'],
        ['w', 'w', 'w', 'g', 'g', 'r', 'r', 'r', 'g', 'w'],
        ['g', 'g', 'g', 'g', 'r', 'r', 'r', 'r', 'g', 'g'],
    ]

    actual = generate_terrain(
        10, 10,
        noise=[(1.0, PerlinNoise(octaves=1.0, seed=123))],
        water_level=0.0,
        rock_level=0.1,
    )

    for i in range(1, len(actual)):
        assert len(actual[i]) == len(
            actual[0]), f"not a rectangle: first row = {len(actual[0])} / {i}-th row = {len(actual[i])}"

    assert len(actual) == len(expected), f"terrain should have {len(expected)} rows, found {len(actual)}"
    assert len(actual[0]) == len(expected[0]), f"terrain should have {len(expected[0])} columns, found {len(actual[0])}"

    for i, (a, e) in enumerate(zip(actual, expected)):
        assert a == e, f"{i}-th row is incorrect, expected {' '.join(e)}, found {' '.join(a)}"


def test_biome():
    from src.project import generate_biome
    expected = [
        ['f', ' ', 'f', 'f', 'f', 'f', ' ', ' ', 'f', 'f'],
        [' ', ' ', ' ', 'f', ' ', ' ', ' ', ' ', 'f', 'f'],
        ['f', 'f', ' ', 'f', ' ', 'f', ' ', ' ', 'f', ' '],
        [' ', 'f', 'f', 'f', 'f', ' ', ' ', 'f', 'f', ' '],
        ['f', 'f', 'f', 'f', ' ', 'f', 'f', 'f', ' ', ' '],
        [' ', 'f', ' ', ' ', 'f', 'f', 'f', 'f', ' ', ' '],
        ['f', 'f', 'f', ' ', ' ', ' ', ' ', ' ', ' ', 'f'],
        ['f', ' ', 'f', ' ', 'f', 'f', ' ', 'f', ' ', ' '],
        [' ', ' ', ' ', ' ', 'f', 'f', 'f', 'f', ' ', 'f'],
        ['f', 'f', ' ', 'f', ' ', ' ', 'f', 'f', ' ', 'f'],
    ]

    actual = generate_biome(
        10, 10,
        noise=[(1.0, PerlinNoise(octaves=5.0, seed=123))],
        forest_level=0.0,
    )

    for i in range(1, len(actual)):
        assert len(actual[i]) == len(
            actual[0]), f"not a rectangle: first row = {len(actual[0])} / {i}-th row = {len(actual[i])}"

    assert len(actual) == len(expected), f"terrain should have {len(expected)} rows, found {len(actual)}"
    assert len(actual[0]) == len(expected[0]), f"terrain should have {len(expected[0])} columns, found {len(actual[0])}"

    for i, (a, e) in enumerate(zip(actual, expected)):
        assert a == e, f"{i}-th row is incorrect, expected {' '.join(e)}, found {' '.join(a)}"
