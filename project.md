# Generating random maps and finding shortest paths [[TOC](README.md#the-map-project)]

## Setting up the code [[TOC](README.md#the-map-project)]
:mag: read the [documentation of `src.ui`]

:file_folder: create a file called `src/project.py`

:pencil: copy paste the example from the [documentation of `src.ui`] into `project.py`

:gear: run `python src/project.py`, you should see some water

## Generating random maps [[TOC](README.md#the-map-project)]
The map is not very interesting... let's improve that! in this project, we will be using
three types of "_land_": _water_, _grass_ and _rock_. They will be reprensented by the letters `w`,
`g` and `r` respectively.

The map will be constructed from two layers:
- the _terrain_ will tell which part of the map has which _land type_ based on the height of the
  terrain, e.g. _water_ will be at the bottom and _rocky_ terrain will be the highest
- the _biome_ will enhance the _terrain_ with some additional information, i.e. whether there is a
  _forest_ or not.

### White noise [[TOC](README.md#the-map-project)]
:pencil: Complete the `generate_terrain` and `generate_biome` functions. They should return matrices
of characters of size `width` by `height`. You can sample random numbers between $0$ and $1$ and
then assign the _land type_ to each cell of the map according to the value, e.g. values below
$\frac{1}{4}$ could be assigned to _water_, values between $\frac{1}{4}$ and $\frac{3}{4}$ could
be assigned to _grass_ and values above $\frac{3}{4}$ could be assigned to _rock_.

:question: How does the maps look like?

### Perlin noise [[TOC](README.md#the-map-project)]
The issue with the _white noise_ we've been using so far is that nearby sampled values don't have
anything in common, thus resulting in a map where nearby cells are not very consistent with each
other.

In order to get nicer looking maps, we will be using a different kind of noise that will ensure
nearby sampled values will be similar! This noise is called [_Perlin noise_](https://en.wikipedia.org/wiki/Perlin_noise)
and we will be using the [`perlin_noise` library](https://pypi.org/project/perlin-noise/).

:mag: Look at the examples in the documentation.

The idea is the following:
- each noise is defined by two numbers:
  - the _amplitude_ of the noise, i.e. how much it weighs
  - the _octaves_, i.e. the frequency of the noise
- noises can then be sampled on the same point and then summed up with their weights, giving a final
  sampled value for the sample point
- in our case, sample points will be points in 2D space, e.g. points whose coordinates are between
  $0$ and $1$

:pencil: Define the noise octaves outside of `generate_terrain` and `generate_biome` in order to
allow changing the octaves and amplitudes of the _Perlin_ noise. You should now be defining
two different noises in your _main_ and pass them to the _generate_ functions.

:pencil: Improve your `generate_terrain` and `generate_biome` functions with _Perlin noise_. They
should:
- sample each noise at every point in the $0$-$1$ square and sum the results with the weights
- based on the final value $v_{x, y}$ for each sample point, assign a different _land type_:
  - if $v_{x, y} \ge 0.1$, assign the _rock_ land type
  - if $v_{x, y} \ge 0$, assign the _grass_ land type
  - otherwise, i.e. if $v_{x, y} \lt 0$, assign the _water_ land type
  - and for the _biome_, assign the _forest_ biome type if $v_{x, y} \ge 0$, represented by the `f`
    character, otherwise, use a _space_ character

:gear: Try with a single octave with frequency $1$ and weight $1$.

:gear: Try adding more octaves with higher frequencies and lower amplitudes. You should get less
flat maps.

### Seeding the generation [[TOC](README.md#the-map-project)]

One thing you might notice is that, every time the code runs, the map is completely different even
though the noise octaves and sample points are the same!

In order to have more reproducible generations, we will freeze the pseudo-random number generator
with a so-called _seed_.

The `PerlinNoise` class constructor takes an additional `seed: int` parameter.

:gear: Fix the seed to `123`.

:question: What happens now when you run the code over and over again?

:pencil: Make sure you get the same map with the following parameters:
- terrain noise with a seed of $123$:
  - $w = 1.0$, $f = 1.0$
  - $w = 0.5$, $f = 3.0$
- biome noise with a seed of $247$:
  - $w = 1.0$, $f = 1.0$
  - $w = 0.5$, $f = 3.0$
- water level: $0$
- rock level: $0.1$
- forest level: $0$

![map](assets/map.png)

## Plugging _shortest path_ algorithms [[TOC](README.md#the-map-project)]
### Dijkstra [[TOC](README.md#the-map-project)]
:pencil: Complete the `disjktra` function with the _Disjktra_ algorithm. This function should return
three lists of nodes, i.e. tuples with two `int` items in them:
- the nodes in the path from start to end, in order, or the empty list if the end node can't be
reached from the start node
- the list of visited nodes in the algorithm
- the _frontier_ of nodes to visit next

:gear: Run `project.py` again.

:question: Does what you see make sense?

:question: Can you climb up or down _rocky_ terrain?

### A* [[TOC](README.md#the-map-project)]
:pencil: Implement the A* algorithm in the `a_star` function. The same conventions as above apply.
You can use the _Euclidean distance_ between two 2D points as the _heuristic_ of A*.

:pencil: Pass the `a_star` function to `canva.setup` instead of `disjktra`.

:gear: Run `project.py` again.

:question: Do you see any difference with the _Disjktra_ algorithm?

:question: Can you design a map manually where A* does not perform very well compared to _Disjktra_?

[documentation of `src.ui`]: src/README.md

---
---
> [go to next](eight-puzzle.md)
