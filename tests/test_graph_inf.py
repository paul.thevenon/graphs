from typing import List, Tuple


def graph(n: int) -> List[Tuple[int, float]]:
    GRAPH = {
        1: {2: 7, 3: 9, 6: 14},
        2: {1: 7, 3: 10, 4: 15},
        3: {1: 9, 2: 10, 4: 11, 6: 2},
        4: {2: 15, 3: 11, 5: 6},
        5: {4: 6, 6: 9},
        6: {1: 14, 3: 2, 5: 10},
    }
    return [(k, v) for k, v in GRAPH[n].items()]


def test_dijkstra():
    from src.graph_inf import dijkstra
    for s, e, expected in [
        (1, 1, [1]),
        (1, 2, [1, 2]),
        (1, 3, [1, 3]),
        (1, 4, [1, 3, 4]),
        (1, 5, [1, 3, 6, 5]),
        (1, 6, [1, 3, 6]),

        (2, 1, [2, 1]),
        (2, 2, [2]),
        (2, 3, [2, 3]),
        (2, 4, [2, 4]),
        (2, 5, [2, 4, 5]),
        (2, 6, [2, 3, 6]),

        (3, 1, [3, 1]),
        (3, 2, [3, 2]),
        (3, 3, [3]),
        (3, 4, [3, 4]),
        (3, 5, [3, 6, 5]),
        (3, 6, [3, 6]),

        (4, 1, [4, 3, 1]),
        (4, 2, [4, 2]),
        (4, 3, [4, 3]),
        (4, 4, [4]),
        (4, 5, [4, 5]),
        (4, 6, [4, 3, 6]),

        (5, 1, [5, 6, 3, 1]),
        (5, 2, [5, 4, 2]),
        (5, 3, [5, 6, 3]),
        (5, 4, [5, 4]),
        (5, 5, [5]),
        (5, 6, [5, 6]),

        (6, 1, [6, 3, 1]),
        (6, 2, [6, 3, 2]),
        (6, 3, [6, 3]),
        (6, 4, [6, 3, 4]),
        (6, 5, [6, 5]),
        (6, 6, [6]),
    ]:
        path = dijkstra(graph, s, e)
        assert path == expected, f"path from {s} to {e} should be {expected}, found {path}"


def test_a_star():
    from src.graph_inf import dijkstra
    from src.graph_inf import a_star

    def h(n1: int, n2: int) -> float:
        weights = {
            1: {1: 0, 2: 1, 3: 1, 4: 2, 5: 2, 6: 1},
            2: {1: 1, 2: 0, 3: 1, 4: 1, 5: 2, 6: 2},
            3: {1: 1, 2: 1, 3: 0, 4: 1, 5: 2, 6: 1},
            4: {1: 2, 2: 1, 3: 1, 4: 0, 5: 1, 6: 2},
            5: {1: 2, 2: 2, 3: 2, 4: 1, 5: 0, 6: 1},
            6: {1: 1, 2: 2, 3: 1, 4: 2, 5: 1, 6: 0},
        }
        return weights[n1][n2]

    n = 6
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            d_actual = dijkstra(graph, i, j)
            a_actual = a_star(graph, i, j, h)
            assert d_actual == a_actual, f"A* should give the same results as Dijkstra for path between {i} and {j}, expected {d_actual}, found {a_actual}"
