from itertools import pairwise


def test_dfs_traversal():
    graph = {
        '1': ['2', '3', '6'],
        '2': ['1', '3', '4'],
        '3': ['1', '2', '4', '6'],
        '4': ['2', '3', '5'],
        '5': ['4', '6'],
        '6': ['1', '3', '5']
    }
    from src.graph import dfs_traversal
    for n, expected in [
        ('1', ['1', '2', '3', '4', '5', '6']),
        ('2', ['2', '1', '3', '4', '5', '6']),
        ('3', ['3', '1', '2', '4', '5', '6']),
        ('4', ['4', '2', '1', '3', '6', '5']),
        ('5', ['5', '4', '2', '1', '3', '6']),
        ('6', ['6', '1', '2', '3', '4', '5']),
    ]:
        res = []
        dfs_traversal(graph, n, lambda n: res.append(n))
        assert res == expected, f"DFS traversal from {n} is incorrect, expected to visit nodes {expected}, found {res}"


def test_dfs():
    graph = {
        '1': ['2', '3', '6'],
        '2': ['1', '3', '4'],
        '3': ['1', '2', '4', '6'],
        '4': ['2', '3', '5'],
        '5': ['4', '6'],
        '6': ['1', '3', '5']
    }
    from src.graph import dfs
    n = 6
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            path = dfs(graph, str(i), str(j))
            if i == j:
                assert path == [str(i)], f"path from {i} to itself should only contain {i}"
            else:
                assert path[0] == str(i), f"path from {i} to {j} should start with {i}, found {path[0]}"
                assert path[-1] == str(j), f"path from {i} to {j} should end with {j}, found {path[-1]}"
                for a, b in pairwise(path):
                    assert a in graph and b in graph[
                        a], f"path from {i} to {j} should contain valid edges, found {a}->{b}"
