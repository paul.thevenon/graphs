import pygame
import rich
from src.tileset import load_tileset, Tile, get_animation_steps, AnimationStep
from copy import deepcopy
from pathlib import Path
import random
from typing import Tuple, List, Callable

BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

ANIMATION_SEQUENCE_LEN = 4
ANIMATION_INV_SPEED = 5


Node = Tuple[int, int]
GetNeighborsFn = Callable[[Node], List[Tuple[Node, float]]]


def info(msg: str, end: str = '\n'):
    rich.print(f"[bold green]INFO[/bold green]: {msg}", end=end)


def warning(msg: str, end: str = '\n'):
    rich.print(f"[bold yellow]WARNING[/bold yellow]: {msg}", end=end)


TILEMAP = {
    # iiii
    "gggg": [
        ("grass_1", None),
        ("grass_2", None),
        ("grass_3", None),
        ("grass_4", None),
        ("grass_5", None),
        ("grass_6", None),
        ("grass_7", None),
        ("grass_8", None),
        ("grass_9", None),
    ],
    "wwww": [("water", None)],
    "rrrr": [("grass_1", None)],

    # iiij
    "gggw": [("river_corner_north_west", None)],  # noqa: E501
    "ggwg": [("river_corner_north_east", None)],  # noqa: E501
    "gwgg": [("river_corner_south_west", None)],  # noqa: E501
    "wggg": [("river_corner_south_east", None)],  # noqa: E501

    "wwwg": [("river_inv_corner_south_east", None)],  # noqa: E501
    "wwgw": [("river_inv_corner_south_west", None)],  # noqa: E501
    "wgww": [("river_inv_corner_north_east", None)],  # noqa: E501
    "gwww": [("river_inv_corner_north_west", None)],  # noqa: E501

    "gggr": [("rock_north_west", None)],  # noqa: E501
    "ggrg": [("rock_north_east", None)],  # noqa: E501
    "grgg": [("rock_south_west", None)],  # noqa: E501
    "rggg": [("rock_south_east", None)],  # noqa: E501

    "wwwr": [("water", "rock_north_west_2")],  # noqa: E501
    "wwrw": [("water", "rock_north_east_2")],  # noqa: E501
    "wrww": [("water", "rock_south_west_2")],  # noqa: E501
    "rwww": [("water", "rock_south_east_2")],  # noqa: E501

    "rrrw": [("water", "rock_corner_south_east_2")],  # noqa: E501
    "rrwr": [("water", "rock_corner_south_west_2")],  # noqa: E501
    "rwrr": [("water", "rock_corner_north_east_2")],  # noqa: E501
    "wrrr": [("water", "rock_corner_north_west_2")],  # noqa: E501

    "rrrg": [("rock_corner_south_east", None)],  # noqa: E501
    "rrgr": [("rock_corner_south_west", None)],  # noqa: E501
    "rgrr": [("rock_corner_north_east", None)],  # noqa: E501
    "grrr": [("rock_corner_north_west", None)],  # noqa: E501

    # iijj
    "wggw": [("river_diag_anti", None)],  # noqa: E501
    "gwwg": [("river_diag", None)],
    "ggww": [("river_north", None)],
    "gwgw": [("river_west", None)],
    "wgwg": [("river_east", None)],
    "wwgg": [("river_south", None)],

    "wrrw": [("water", "rock_diag_anti_2")],  # noqa: E501
    "rwwr": [("water", "rock_diag_2")],
    "rrww": [("water", "rock_south_2")],
    "rwrw": [("water", "rock_east_2")],
    "wrwr": [("water", "rock_west_2")],
    "wwrr": [("water", "rock_north_2")],

    "rggr": [("rock_diag", None)],
    "grrg": [("rock_diag_anti", None)],
    "ggrr": [("rock_north", None)],
    "grgr": [("rock_west", None)],
    "rgrg": [("rock_east", None)],
    "rrgg": [("rock_south", None)],

    # iijk
    "wwgr": [("river_south", "rock_north_west_2")],  # noqa: E501
    "gwrw": [("river_west", "rock_north_east_2")],  # noqa: E501
    "rgww": [("river_north", "rock_south_east_2")],  # noqa: E501
    "wrwg": [("river_east", "rock_south_west_2")],  # noqa: E501

    "wwrg": [("river_south", "rock_north_east_2")],  # noqa: E501
    "rwgw": [("river_west", "rock_south_east_2")],  # noqa: E501
    "grww": [("river_north", "rock_south_west_2")],  # noqa: E501
    "wgwr": [("river_east", "rock_north_west_2")],  # noqa: E501

    "rrwg": [("river_east", "rock_south_2")],  # noqa: E501
    "wrgr": [("river_corner_south_east", "rock_west_2")],  # noqa: E501
    "gwrr": [("river_west", "rock_north_2")],  # noqa: E501
    "rgrw": [("river_corner_north_west", "rock_east_2")],  # noqa: E501

    "rrgw": [("river_corner_north_west", "rock_south_2")],  # noqa: E501
    "grwr": [("river_corner_north_east", "rock_west_2")],  # noqa: E501
    "wgrr": [("river_east", "rock_north_2")],  # noqa: E501
    "rwrg": [("river_south", "rock_east_2")],  # noqa: E501

    "ggwr": [("river_corner_north_east", "rock_north_west_2")],  # noqa: E501
    "wgrg": [("river_corner_south_east", "rock_north_east_2")],  # noqa: E501
    "rwgg": [("river_corner_south_west", "rock_south_east_2")],  # noqa: E501
    "grgw": [("river_corner_north_west", "rock_south_west_2")],  # noqa: E501

    "ggrw": [("river_corner_north_west", "rock_north_east_2")],  # noqa: E501
    "rgwg": [("river_corner_north_east", "rock_south_east_2")],  # noqa: E501
    "wrgg": [("river_corner_south_east", "rock_south_west_2")],  # noqa: E501
    "gwgr": [("river_corner_south_west", "rock_north_west_2")],  # noqa: E501

    "gwrg": [("river_corner_south_west", "rock_north_east_2")],  # noqa: E501
    "rggw": [("river_corner_north_west", "rock_south_east_2")],  # noqa: E501
    "grwg": [("river_corner_north_east", "rock_south_west_2")],  # noqa: E501
    "wggr": [("river_corner_south_east", "rock_north_west_2")],  # noqa: E501

    "wgrw": [("river_inv_corner_north_east", "rock_north_east_2")],  # noqa: E501
    "rwwg": [("river_inv_corner_south_east", "rock_south_east_2")],  # noqa: E501
    "wrgw": [("river_inv_corner_south_west", "rock_south_west_2")],  # noqa: E501
    "gwwr": [("river_inv_corner_north_west", "rock_north_west_2")],  # noqa: E501

    "rwgr": [("river_corner_south_west", "rock_diag_2")],  # noqa: E501
    "grrw": [("river_corner_north_west", "rock_diag_anti_2")],  # noqa: E501
    "rgwr": [("river_corner_north_east", "rock_diag_2")],  # noqa: E501
    "wrrg": [("river_corner_south_east", "rock_diag_anti_2")],  # noqa: E501
}

FOREST_TILEMAP = {
    # full forest
    "111111111": ["forest"],

    # exactly 4 in the corners
    "000011011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "000110110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "011011000": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "110110000": ["forest_corner_south_east", "forest_corner_in_south_east"],

    "000110111": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "000111110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "001110110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "001110111": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "001111110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "010110110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "010110111": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "010111110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "011110110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "011110111": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "100110110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "100110111": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "100111110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "101110110": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "101110111": ["forest_corner_north_east", "forest_corner_in_north_east"],
    "101111110": ["forest_corner_north_east", "forest_corner_in_north_east"],

    "011011001": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011011010": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011011100": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011011101": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011011110": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011111000": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011111001": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011111010": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011111100": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "011111101": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "111011000": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "111011001": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "111011010": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "111011100": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "111011101": ["forest_corner_south_west", "forest_corner_in_south_west"],
    "111011110": ["forest_corner_south_west", "forest_corner_in_south_west"],

    "110110001": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110110010": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110110011": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110110100": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110110101": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110111000": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110111001": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110111010": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110111100": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "110111101": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "111110000": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "111110001": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "111110010": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "111110011": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "111110100": ["forest_corner_south_east", "forest_corner_in_south_east"],
    "111110101": ["forest_corner_south_east", "forest_corner_in_south_east"],

    "000011111": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "000111011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "001011011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "001011111": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "001111011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "010011011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "010011111": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "010111011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "100011011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "100011111": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "100111011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "101011011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "101011111": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "101111011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "110011011": ["forest_corner_north_west", "forest_corner_in_north_west"],
    "110011111": ["forest_corner_north_west", "forest_corner_in_north_west"],

    # exact cardinal directions
    "000111111": ["forest_north"],
    "111111000": ["forest_south"],
    "110110110": ["forest_east"],
    "011011011": ["forest_west"],

    "001111111": ["forest_north"],
    "010111111": ["forest_north"],
    "101111111": ["forest_north"],
    "100111111": ["forest_north"],

    "111111001": ["forest_south"],
    "111111010": ["forest_south"],
    "111111101": ["forest_south"],
    "111111100": ["forest_south"],

    "110110111": ["forest_east"],
    "110111110": ["forest_east"],
    "111110110": ["forest_east"],
    "111110111": ["forest_east"],

    "011011111": ["forest_west"],
    "011111011": ["forest_west"],
    "111011011": ["forest_west"],
    "111011111": ["forest_west"],

    "011111111": ["forest_corner_inv_north_west"],
    "110111111": ["forest_corner_inv_north_east"],
    "111111110": ["forest_corner_inv_south_east"],
    "111111011": ["forest_corner_inv_south_west"],

    "110111011": ["forest_diag"],
    "011111110": ["forest_diag_anti"],

    # 3 or less
    "000010000": ["tree_1", "tree_2", "tree_3"],
    "000010001": ["tree_1", "tree_2", "tree_3"],
    "000010010": ["tree_1", "tree_2", "tree_3"],
    "000010011": ["tree_1", "tree_2", "tree_3"],
    "000010100": ["tree_1", "tree_2", "tree_3"],
    "000010101": ["tree_1", "tree_2", "tree_3"],
    "000010110": ["tree_1", "tree_2", "tree_3"],
    "000011000": ["tree_1", "tree_2", "tree_3"],
    "000011001": ["tree_1", "tree_2", "tree_3"],
    "000011010": ["tree_1", "tree_2", "tree_3"],
    "000011100": ["tree_1", "tree_2", "tree_3"],
    "000110000": ["tree_1", "tree_2", "tree_3"],
    "000110001": ["tree_1", "tree_2", "tree_3"],
    "000110010": ["tree_1", "tree_2", "tree_3"],
    "000110100": ["tree_1", "tree_2", "tree_3"],
    "000111000": ["tree_1", "tree_2", "tree_3"],
    "001010000": ["tree_1", "tree_2", "tree_3"],
    "001010001": ["tree_1", "tree_2", "tree_3"],
    "001010010": ["tree_1", "tree_2", "tree_3"],
    "001010100": ["tree_1", "tree_2", "tree_3"],
    "001011000": ["tree_1", "tree_2", "tree_3"],
    "001110000": ["tree_1", "tree_2", "tree_3"],
    "010010000": ["tree_1", "tree_2", "tree_3"],
    "010010001": ["tree_1", "tree_2", "tree_3"],
    "010010010": ["tree_1", "tree_2", "tree_3"],
    "010010100": ["tree_1", "tree_2", "tree_3"],
    "010011000": ["tree_1", "tree_2", "tree_3"],
    "010110000": ["tree_1", "tree_2", "tree_3"],
    "011010000": ["tree_1", "tree_2", "tree_3"],
    "100010000": ["tree_1", "tree_2", "tree_3"],
    "100010001": ["tree_1", "tree_2", "tree_3"],
    "100010010": ["tree_1", "tree_2", "tree_3"],
    "100010100": ["tree_1", "tree_2", "tree_3"],
    "100011000": ["tree_1", "tree_2", "tree_3"],
    "100110000": ["tree_1", "tree_2", "tree_3"],
    "101010000": ["tree_1", "tree_2", "tree_3"],
    "110010000": ["tree_1", "tree_2", "tree_3"],

    # exactly 4 without the corners
    "000010111": ["tree_1", "tree_2", "tree_3"],
    "000011101": ["tree_1", "tree_2", "tree_3"],
    "000011110": ["tree_1", "tree_2", "tree_3"],
    "000110011": ["tree_1", "tree_2", "tree_3"],
    "000110101": ["tree_1", "tree_2", "tree_3"],
    "000111001": ["tree_1", "tree_2", "tree_3"],
    "000111010": ["tree_1", "tree_2", "tree_3"],
    "000111100": ["tree_1", "tree_2", "tree_3"],
    "001010011": ["tree_1", "tree_2", "tree_3"],
    "001010101": ["tree_1", "tree_2", "tree_3"],
    "001010110": ["tree_1", "tree_2", "tree_3"],
    "001011001": ["tree_1", "tree_2", "tree_3"],
    "001011010": ["tree_1", "tree_2", "tree_3"],
    "001011100": ["tree_1", "tree_2", "tree_3"],
    "001110001": ["tree_1", "tree_2", "tree_3"],
    "001110010": ["tree_1", "tree_2", "tree_3"],
    "001110100": ["tree_1", "tree_2", "tree_3"],
    "001111000": ["tree_1", "tree_2", "tree_3"],
    "010010011": ["tree_1", "tree_2", "tree_3"],
    "010010101": ["tree_1", "tree_2", "tree_3"],
    "010010110": ["tree_1", "tree_2", "tree_3"],
    "010011001": ["tree_1", "tree_2", "tree_3"],
    "010011010": ["tree_1", "tree_2", "tree_3"],
    "010011100": ["tree_1", "tree_2", "tree_3"],
    "010110001": ["tree_1", "tree_2", "tree_3"],
    "010110010": ["tree_1", "tree_2", "tree_3"],
    "010110100": ["tree_1", "tree_2", "tree_3"],
    "010111000": ["tree_1", "tree_2", "tree_3"],
    "011010001": ["tree_1", "tree_2", "tree_3"],
    "011010010": ["tree_1", "tree_2", "tree_3"],
    "011010100": ["tree_1", "tree_2", "tree_3"],
    "011110000": ["tree_1", "tree_2", "tree_3"],
    "100010011": ["tree_1", "tree_2", "tree_3"],
    "100010101": ["tree_1", "tree_2", "tree_3"],
    "100010110": ["tree_1", "tree_2", "tree_3"],
    "100011001": ["tree_1", "tree_2", "tree_3"],
    "100011010": ["tree_1", "tree_2", "tree_3"],
    "100011100": ["tree_1", "tree_2", "tree_3"],
    "100110001": ["tree_1", "tree_2", "tree_3"],
    "100110010": ["tree_1", "tree_2", "tree_3"],
    "100110100": ["tree_1", "tree_2", "tree_3"],
    "100111000": ["tree_1", "tree_2", "tree_3"],
    "101010001": ["tree_1", "tree_2", "tree_3"],
    "101010010": ["tree_1", "tree_2", "tree_3"],
    "101010100": ["tree_1", "tree_2", "tree_3"],
    "101011000": ["tree_1", "tree_2", "tree_3"],
    "101110000": ["tree_1", "tree_2", "tree_3"],
    "110010001": ["tree_1", "tree_2", "tree_3"],
    "110010010": ["tree_1", "tree_2", "tree_3"],
    "110010100": ["tree_1", "tree_2", "tree_3"],
    "110011000": ["tree_1", "tree_2", "tree_3"],
    "111010000": ["tree_1", "tree_2", "tree_3"],

    # exactly 5 without the ones grouped together
    "000111101": ["tree_1", "tree_2", "tree_3"],
    "001010111": ["tree_1", "tree_2", "tree_3"],
    "001011101": ["tree_1", "tree_2", "tree_3"],
    "001011110": ["tree_1", "tree_2", "tree_3"],
    "001110011": ["tree_1", "tree_2", "tree_3"],
    "001110101": ["tree_1", "tree_2", "tree_3"],
    "001111001": ["tree_1", "tree_2", "tree_3"],
    "001111010": ["tree_1", "tree_2", "tree_3"],
    "001111100": ["tree_1", "tree_2", "tree_3"],
    "010010111": ["tree_1", "tree_2", "tree_3"],
    "010011101": ["tree_1", "tree_2", "tree_3"],
    "010011110": ["tree_1", "tree_2", "tree_3"],
    "010110011": ["tree_1", "tree_2", "tree_3"],
    "010110101": ["tree_1", "tree_2", "tree_3"],
    "010111001": ["tree_1", "tree_2", "tree_3"],
    "010111010": ["tree_1", "tree_2", "tree_3"],
    "010111100": ["tree_1", "tree_2", "tree_3"],
    "011010011": ["tree_1", "tree_2", "tree_3"],
    "011010101": ["tree_1", "tree_2", "tree_3"],
    "011010110": ["tree_1", "tree_2", "tree_3"],
    "011110001": ["tree_1", "tree_2", "tree_3"],
    "011110010": ["tree_1", "tree_2", "tree_3"],
    "011110100": ["tree_1", "tree_2", "tree_3"],
    "100010111": ["tree_1", "tree_2", "tree_3"],
    "100011101": ["tree_1", "tree_2", "tree_3"],
    "100011110": ["tree_1", "tree_2", "tree_3"],
    "100110011": ["tree_1", "tree_2", "tree_3"],
    "100110101": ["tree_1", "tree_2", "tree_3"],
    "100111001": ["tree_1", "tree_2", "tree_3"],
    "100111010": ["tree_1", "tree_2", "tree_3"],
    "100111100": ["tree_1", "tree_2", "tree_3"],
    "101010011": ["tree_1", "tree_2", "tree_3"],
    "101010101": ["tree_1", "tree_2", "tree_3"],
    "101010110": ["tree_1", "tree_2", "tree_3"],
    "101011001": ["tree_1", "tree_2", "tree_3"],
    "101011010": ["tree_1", "tree_2", "tree_3"],
    "101011100": ["tree_1", "tree_2", "tree_3"],
    "101110001": ["tree_1", "tree_2", "tree_3"],
    "101110010": ["tree_1", "tree_2", "tree_3"],
    "101110100": ["tree_1", "tree_2", "tree_3"],
    "101111000": ["tree_1", "tree_2", "tree_3"],
    "110010011": ["tree_1", "tree_2", "tree_3"],
    "110010101": ["tree_1", "tree_2", "tree_3"],
    "110010110": ["tree_1", "tree_2", "tree_3"],
    "110011001": ["tree_1", "tree_2", "tree_3"],
    "110011010": ["tree_1", "tree_2", "tree_3"],
    "110011100": ["tree_1", "tree_2", "tree_3"],
    "111010001": ["tree_1", "tree_2", "tree_3"],
    "111010010": ["tree_1", "tree_2", "tree_3"],
    "111010100": ["tree_1", "tree_2", "tree_3"],

    # 6 or more
    "001111101": ["tree_1", "tree_2", "tree_3"],
    "010111101": ["tree_1", "tree_2", "tree_3"],
    "011010111": ["tree_1", "tree_2", "tree_3"],
    "011110011": ["tree_1", "tree_2", "tree_3"],
    "011110101": ["tree_1", "tree_2", "tree_3"],
    "100111101": ["tree_1", "tree_2", "tree_3"],
    "101010111": ["tree_1", "tree_2", "tree_3"],
    "101011101": ["tree_1", "tree_2", "tree_3"],
    "101011110": ["tree_1", "tree_2", "tree_3"],
    "101110011": ["tree_1", "tree_2", "tree_3"],
    "101110101": ["tree_1", "tree_2", "tree_3"],
    "101111001": ["tree_1", "tree_2", "tree_3"],
    "101111010": ["tree_1", "tree_2", "tree_3"],
    "101111100": ["tree_1", "tree_2", "tree_3"],
    "101111101": ["tree_1", "tree_2", "tree_3"],
    "110010111": ["tree_1", "tree_2", "tree_3"],
    "110011101": ["tree_1", "tree_2", "tree_3"],
    "110011110": ["tree_1", "tree_2", "tree_3"],
    "111010011": ["tree_1", "tree_2", "tree_3"],
    "111010101": ["tree_1", "tree_2", "tree_3"],
    "111010110": ["tree_1", "tree_2", "tree_3"],
    "111010111": ["tree_1", "tree_2", "tree_3"],
}


def check_terrain(terrain: List[List[str]], *, biome: List[List[str]]):
    for i in range(1, len(terrain)):
        assert len(terrain[0]) == len(terrain[i]), f"terrain is not a rectangle, (first row: {len(terrain[0])} / {i}-th row: {len(terrain[i])})"
    for i in range(1, len(biome)):
        assert len(biome[0]) == len(biome[i]), f"biome is not a rectangle, (first row: {len(biome[0])} / {i}-th row: {len(biome[i])})"
    assert len(terrain) - 1 == len(biome), f"biome should be one row smaller than terrain (terrain: {len(terrain)} / biome: {len(biome)})"
    assert len(terrain[0]) - 1 == len(biome[0]), f"biome should be one column smaller than terrain (terrain: {len(terrain[0])} / biome: {len(biome[0])})"


def show_grid(grid: List[List[str]]):
    print('+' + '-' * (2 * len(grid[0]) - 1) + '+')
    for row in grid:
        print('|' + ' '.join(list(row)) + '|')
    print('+' + '-' * (2 * len(grid[0]) - 1) + '+')


def build_image(
    terrain: List[List[str]],
    biome: List[List[str]],
    *,
    tiles: List[Tile],
    image_size: (int, int),
    s: int,
    t: int,
    animations: List[AnimationStep],
    seed: int,
) -> pygame.surface.Surface:
    h, w = len(terrain), len(terrain[0])

    image = pygame.Surface(image_size, pygame.SRCALPHA)

    random.seed(seed)

    incomplete, bad_tile = False, None
    for i in range(1, h - 2):
        for j in range(1, w - 2):
            nw = terrain[i][j]
            ne = terrain[i][j + 1]
            sw = terrain[i + 1][j]
            se = terrain[i + 1][j + 1]
            key = ''.join([nw, ne, sw, se])

            bg, fg = random.choice(TILEMAP.get(key, [("spell_red", None)]))

            cell_pos = ((j - 1) * s, (i - 1) * s)

            forest = [
                (
                    biome[a][b] == 'f' and
                    terrain[a][b] ==
                    terrain[a][b + 1] ==
                    terrain[a + 1][b] ==
                    terrain[a + 1][b + 1] and
                    terrain[a][b] in ['r', 'g']
                )
                for a, b in [
                    (i - 1, j - 1), (i - 1, j), (i - 1, j + 1),
                    (i, j - 1),     (i, j),     (i, j + 1),
                    (i + 1, j - 1), (i + 1, j), (i + 1, j + 1),
                ]
            ]

            if forest[4]:
                fkey = ''.join(map(str, map(int, forest)))
                fg = random.choice(FOREST_TILEMAP.get(fkey, ["spell_red"]))

            try:
                tile = get_animation_steps(tiles[bg].id, animations)[
                    (t // ANIMATION_INV_SPEED) % ANIMATION_SEQUENCE_LEN
                ].tile
            except Exception:
                tile = tiles[bg]
            image.blit(pygame.transform.scale(tile.image, (s, s)), cell_pos)
            if fg is not None:
                image.blit(
                    pygame.transform.scale(tiles[fg].image, (s, s)),
                    cell_pos,
                )

            if key not in TILEMAP:
                incomplete, bad_tile = True, (nw, ne, sw, se)

    if incomplete:
        warning(f"generation is incomplete with {bad_tile}")

    return image


def render_path_and_points(
    path: List[Node],
    *,
    visited: List[Node],
    next: List[Node],
    width: int = 8,
    alpha: int = 150,
    s: int,
    screen: pygame.surface.Surface,
) -> pygame.surface.Surface:
    path = [(j * s, i * s) for i, j in path]
    visited = [(j * s, i * s) for i, j in visited]
    next = [(j * s, i * s) for i, j in next]

    surf = pygame.Surface(screen.get_size(), pygame.SRCALPHA)

    if len(path) >= 2:
        pygame.draw.lines(
            surf, RED + (alpha,), closed=False, points=path, width=width
        )

    for v in visited:
        pygame.draw.circle(surf, GREEN + (alpha / 2,), v, radius=2*width)
    for n in next:
        pygame.draw.circle(surf, BLUE + (alpha / 4,), n, radius=2*width)
    for p in path:
        pygame.draw.circle(surf, RED + (alpha,), p, radius=2*width)

    return surf


def get_edge_weight(terrain1: str, terrain2: str, biome1: str, biome2: str) -> float | None:
    weights = {
        ('r', 'r'): 1,
        ('r', 'w'): float("inf"),
        ('w', 'r'): float("inf"),
        ('w', 'w'): 3,
        ('r', 'g'): float("inf"),
        ('g', 'r'): float("inf"),
        ('g', 'g'): 1,
        ('g', 'w'): 1,
        ('w', 'g'): 1,
    }
    w = weights.get((terrain1, terrain2))

    if w is not None and (biome1 == 'f' or biome2 == 'f'):
        return 4 * w
    else:
        return w


def create_graph_from_grid(
    terrain: List[List[str]], biome: List[List[str]]
) -> GetNeighborsFn:
    def get_neighbors(n: (int, int)) -> List[Tuple[Node, float]]:
        h, w = len(terrain), len(terrain[0])
        i, j = n

        neighbors = []
        for d, (a, b) in [
            (1.41, (i - 1, j - 1)),
            (1.00, (i - 1, j)),
            (1.41, (i - 1, j + 1)),
            (1.00, (i, j - 1)),
            (1.00, (i, j + 1)),
            (1.41, (i + 1, j - 1)),
            (1.00, (i + 1, j)),
            (1.41, (i + 1, j + 1)),
        ]:
            if 0 <= a < h and 0 <= b < w:
                weight = d * get_edge_weight(
                    terrain[a][b],
                    terrain[i][j],
                    biome[a][b],
                    biome[i][j],
                )
                neighbors.append(((a, b), weight))

        return neighbors

    return get_neighbors


class Canvas:
    def __init__(self, grid_size: (int, int), tile_size: int, frame_rate: int, seed: int):
        self.tile_size = tile_size
        self.frame_rate = frame_rate
        self.seed = seed

        h, w = grid_size
        self.window_size = self.tile_size * (w - 1), self.tile_size * (h - 1)

    def setup(
        self,
        shortest_path_fn: Callable[
            [GetNeighborsFn, Node, Node],
            Tuple[
                List[Node],
                List[Node],
                List[Node],
            ],
        ],
        terrain: List[List[str]],
        biome: List[List[str]],
        graph: GetNeighborsFn,
    ):
        pygame.init()
        self.screen = pygame.display.set_mode(self.window_size)
        self.clock = pygame.time.Clock()
        self.dt = 0

        self.running = True
        self.t = 0
        self.start, self.end, self.path = None, None, None

        self.shortest_path_fn = shortest_path_fn

        self.tiles, self.animations, _ = load_tileset(
            Path("punyworld.json"), do_load_characters=False
        )

        self.terrain = deepcopy(terrain)
        self.biome = biome
        self.graph = graph

        self.terrain.insert(0, deepcopy(self.terrain[0]))
        self.terrain.append(deepcopy(self.terrain[-1]))
        self.terrain.append(deepcopy(self.terrain[-1]))
        for i in range(len(self.terrain)):
            self.terrain[i].insert(0, self.terrain[i][0])
            self.terrain[i].append(self.terrain[i][-1])
            self.terrain[i].append(self.terrain[i][-1])

        self.biome.insert(0, deepcopy(self.biome[0]))
        self.biome.append(deepcopy(self.biome[-1]))
        self.biome.append(deepcopy(self.biome[-1]))
        for i in range(len(self.biome)):
            self.biome[i].insert(0, self.biome[i][0])
            self.biome[i].append(self.biome[i][-1])
            self.biome[i].append(self.biome[i][-1])

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (
                event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE
            ):
                self.running = False
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == pygame.BUTTON_LEFT:
                    x, y = event.pos
                    self.start = (
                        int((y + self.tile_size / 2) // self.tile_size),
                        int((x + self.tile_size / 2) // self.tile_size),
                    )
                elif event.button == pygame.BUTTON_RIGHT:
                    self.start, self.end, self.path = None, None, None
            elif event.type == pygame.MOUSEMOTION:
                x, y = event.pos
                self.end = (
                    int((y + self.tile_size / 2) // self.tile_size),
                    int((x + self.tile_size / 2) // self.tile_size),
                )

    def render(self):
        self.screen.fill(BLACK)
        self.screen.blit(
            build_image(
                self.terrain,
                self.biome,
                tiles=self.tiles,
                s=self.tile_size,
                image_size=self.window_size,
                t=self.t,
                animations=self.animations,
                seed=self.seed,
            ),
            (0, 0),
        )

        if self.start is not None and self.end is not None:
            self.path, self.visited, self.next = self.shortest_path_fn(
                self.graph, self.start, self.end
            )

        if self.path is not None:
            self.screen.blit(
                render_path_and_points(
                    self.path,
                    visited=self.visited,
                    next=self.next,
                    width=5,
                    s=self.tile_size,
                    screen=self.screen,
                ),
                (0, 0, *self.screen.get_size()),
            )

        pygame.display.flip()

    def loop(self):
        while self.running:
            self.handle_events()
            self.render()
            self.dt = self.clock.tick(self.frame_rate) / 1000

            self.t += 1

        pygame.quit()
