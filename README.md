# Graphs

In this class, you will learn about another very common algorithmic tool: graphs.
Some fundamental algorithms to find the shortest path between nodes in the graphs
will be presented and implemented.

Finally, a few examples of graphs will be showcased through a few projects:
- finding the shortest path in a 2D random map generated proceduraly using _Perlin_ noise
- solving the $n$-puzzle game

## Table of content
- [_Upload your work to the LMS_](#upload-your-work-to-the-lms-toc)
- [_Graph Representation_](#graph-representation-toc)
  - [_Question 0_](#question-0-toc)
- [_Graph Operations_](#graph-operations-toc)
  - [_Question 1_](#question-1-toc)
  - [_Question 2_](#question-2-toc)
  - [_Question 3_](#question-3-toc)
- [_Graph Traversal_](#graph-traversal-toc)
  - [_BFS_](#bfs-toc)
    - [_Question 4_](#question-4-toc)
    - [_Question 5_](#question-5-toc)
  - [_DFS_](#dfs-toc)
    - [_Question 6_](#question-6-toc)
    - [_Question 7_](#question-7-toc)
- [_Weighted Graphs_](#weighted-graphs-toc)
  - [_Weighted Graph Representation_](#weighted-graph-representation-toc)
  - [_Weighted Graph Operations_](#weighted-graph-operations-toc)
    - [_Dijkstra's Algorithm_](#dijkstras-algorithm-toc)
      - [_Question 8_](#question-8-toc)
      - [_Question 9_](#question-9-toc)
      - [_Question 10_](#question-10-toc)
    - [_A* Algorithm_](#a-algorithm-toc)
      - [_Formalizing the algorithm_](#formalizing-the-algorithm-toc)
      - [_Question 11_](#question-11-toc)
- [_Very large or infinite graphs_](#very-large-or-infinite-graphs-toc)
  - [_A monster is lurking..._](#a-monster-is-lurking-toc)
  - [_Generating a graph _on the fly__](#generating-a-graph-on-the-fly-toc)
  - [_Question 12_](#question-12-toc)
  - [_Question 13_](#question-13-toc)

### The map project
- [_Setting up the code_](project.md#setting-up-the-code-toc)
- [_Generating random maps_](project.md#generating-random-maps-toc)
  - [_White noise_](project.md#white-noise-toc)
  - [_Perlin noise_](project.md#perlin-noise-toc)
  - [_Seeding the generation_](project.md#seeding-the-generation-toc)
- [_Plugging _shortest path_ algorithms_](project.md#plugging-shortest-path-algorithms-toc)
  - [_Dijkstra_](project.md#dijkstra-toc)
  - [_A*_](project.md#a-toc)

### The 8-puzzle (:label: _optional_)
- [_The problem_](eight-puzzle.md#the-problem-toc)
    - [_Question 0_](eight-puzzle.md#question-0-toc)
- [_Displaying the board_](eight-puzzle.md#displaying-the-board-toc)
    - [_Question 1_](eight-puzzle.md#question-1-toc)
- [_Shuffling the board_](eight-puzzle.md#shuffling-the-board-toc)
    - [_Question 2_](eight-puzzle.md#question-2-toc)
    - [_Displaying the shuffled board_](eight-puzzle.md#displaying-the-shuffled-board-toc)
        - [_Question 3_](eight-puzzle.md#question-3-toc)
- [_Solving the puzzle_](eight-puzzle.md#solving-the-puzzle-toc)
    - [_Question 4_](eight-puzzle.md#question-4-toc)
    - [_Hashing a `Puzzle`_](eight-puzzle.md#hashing-a-puzzle-toc)
        - [_Question 5_](eight-puzzle.md#question-5-toc)
    - [_The possible moves_](eight-puzzle.md#the-possible-moves-toc)
        - [_Question 6_](eight-puzzle.md#question-6-toc)
- [_Plugging the search algorithms_](eight-puzzle.md#plugging-the-search-algorithms-toc)
    - [_Question 7_](eight-puzzle.md#question-7-toc)
    - [_Validate the path_](eight-puzzle.md#validate-the-path-toc)
        - [_Question 8_](eight-puzzle.md#question-8-toc)
    - [_Displaying the path_](eight-puzzle.md#displaying-the-path-toc)
        - [_Question 9_](eight-puzzle.md#question-9-toc)
- [_A* Search and the distance_](eight-puzzle.md#a-search-and-the-distance-toc)
    - [_The Manhattan distance_](eight-puzzle.md#the-manhattan-distance-toc)
        - [_Question 10_](eight-puzzle.md#question-10-toc)
    - [_Applying the A* search algorithm_](eight-puzzle.md#applying-the-a-search-algorithm-toc)
        - [_Question 11_](eight-puzzle.md#question-11-toc)
    - [_Displaying the solution_](eight-puzzle.md#displaying-the-solution-toc)
        - [_Question 12_](eight-puzzle.md#question-12-toc)
- [_Generalizing the puzzle_](eight-puzzle.md#generalizing-the-puzzle-toc)
    - [_Question 13_](eight-puzzle.md#question-13-toc)

![graph](assets/graph.png)

---

## Upload your work to the LMS [[toc](#table-of-content)]
- open a terminal
- go into the folder containing your project
- use the `zip` command to compress your project
```shell
zip -r project.zip . -x "venv/**" ".git/**"
```
- upload the ZIP archive to the [LMS](https://lms.isae.fr/mod/assign/view.php?id=116609&action=editsubmission)

---

## Preparing the code

In this class, we will need some extra dependencies compared to the normal dev environment.

In order to do that, and to bypass the fact that we don't have the permissions to install our own
Python packages in the environment of the class, we will create a Python _virtual environment_.

A _virtual environment_ is just something that will help us isolate dependencies from the main
system.

You can run the following commands, in the directory of the class, and before running `code .` or
any editor you like:
```shell
python -m venv venv       # create a _virtual environment_ in `./venv/`
source venv/bin/activate  # enter the _venv_, you should see `(venv)` in the prompt of your shell

pip install -r requirements.txt # install all the dependencies for this class
```

Next time you want to work on this class, don't forget to run `source venv/bin/activate` :wink:

## Graph Representation [[TOC](#table-of-content)]

A graph is a data structure that consists of a possibly infinite set of nodes or vertices and a set of edges connecting them.

There are two common ways to represent a graph: adjacency matrix and adjacency list.

Let's consider the following graph:
```mermaid
flowchart LR
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    E --- F
```
It has six nodes $(A, B, C, D, E, F)$ and six edges connecting them.

Let's have a look at the two possible representations of this graph, written in Python code:

```python
# adjacency matrix
graph = [
    [0, 1, 1, 0, 0, 0],
    [1, 0, 0, 1, 1, 0],
    [1, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 1],
    [0, 0, 1, 0, 1, 0],
]
```

> :bulb: **Note**
>
> because the example graph is not _directed_, i.e. an edge between $A$ and $B$ both means that $A$
> is connected to $B$ and vice-versa, the _adjacency matrix_ is symmetric.

```python
# adjacency list
my_graph = {
    'A': ['B', 'C'],
    'B': ['A', 'D', 'E'],
    'C': ['A', 'F'],
    'D': ['B'],
    'E': ['B', 'F'],
    'F': ['C', 'E']
}
```

> :exclamation: **Important**
>
> in the rest of this class and in particular in the tests, the _adjacency list_ representation
> will be used because it is generally easier to work with.

### Question 0 [[TOC](#table-of-content)]
:file_folder: Create a file called `src/graph.py`. You will be implementing the following questions in
this file.

> :exclamation: **Important**
>
> in the following of this class, some type annotations will be used to help you see what each
> function expects as arguments and what the return value should look like.
>
> `Node` will be used to denote any _node_ in a graph, e.g. it would be `str` in the example graph
> above where nodes are labeled with letters, but it could be anything you like (as long as it can
> be used as a key in a dictionary :wink:)

:pencil: Add the following snippet at the top of your new file, which will allow writing type annotations:
```python
from typing import Dict, List, Tuple, Callable, Any

Node = str
```

## Graph Operations [[TOC](#table-of-content)]

Now, let's implement some basic operations on graphs. We first need to define our graph.
In a main guard, define the graph as follows:
```python
my_graph = {
  'A': ['B', 'C'],
  'B': ['A', 'D', 'E'],
  'C': ['A', 'F'],
  'D': ['B'],
  'E': ['B', 'F'],
  'F': ['C', 'E']
}
```

### Question 1 [[TOC](#table-of-content)]

:pencil: Write a function that receives a graph as a dictionary and returns the number of nodes and edges.

The function should return a tuple with the number of nodes and edges and below is an example of
what it should look like.
```python
def count_nodes_edges(graph: Dict[Node, List[Node]]) -> (int, int):
    return -1, -1
```

Try to call the function with the graph `my_graph` defined above and print the result.

:gear: You can use `pytest -k "count"` to run the test.

### Question 2 [[TOC](#table-of-content)]

The degree of a node is the number of edges that either go into or out of the node.

:pencil: Write a function that receives a graph as a dictionary and returns the degree of a node.
```python
def degree(graph: Dict[Node, List[Node]], node: Node) -> int:
    return -1
```

Try to call the function with the graph `my_graph` defined above and print the result for the node `B`.

:gear: You can use `pytest -k "degree"` to run the test.

### Question 3 [[TOC](#table-of-content)]

The neighbors of a node are the nodes that are connected to it by an edge.

:pencil: Write a function that receives a graph as a dictionary and returns the neighbors of a node,
with the associated _weights_.

```python
def neighbors(graph: Dict[Node, List[Node]], node: Node) -> List[Node]:
    return []
```

Try to call the function with the graph `my_graph` defined above and print the result for the node `B`.

:gear: You can use `pytest -k "test_graph_neighbors"` to run the test.

:gear: You should now be able to run `pytest -k test_graph.py` and see green.

## Graph Traversal [[TOC](#table-of-content)]

Graph traversal is the process of visiting all the nodes of a graph.

### BFS [[TOC](#table-of-content)]

**B**readth-**F**irst **S**earch (**BFS**) is a fundamental graph traversal algorithm used to explore nodes and edges of a graph.
It's called "_breadth-first_" because it explores all the nodes at the present depth before moving on to the nodes at the next depth.

#### Question 4 [[TOC](#table-of-content)]

:pencil: Implement the BFS algorithm on a graph.

The `graph` parameter is a dictionary where the keys are the nodes and the values are lists of adjacent nodes.
The `start` parameter is the node where to start the traversal.
The `fn` parameter is a _callback_ function that takes a node and performs any operation, e.g. this function could be a simple `print`.
The function should apply the `fn` parameter to each node visited during the traversal.

```python
def bfs_traversal(graph: Dict[Node, List[Node]], start: Node, fn: Callable[[Node], Any]):
    ...
```

:gear: You can use `pytest -k "bfs and traversal"` to run the test.

#### Question 5 [[TOC](#table-of-content)]

A special case of the BFS traversal algorithm is to compute the path between two nodes in a graph.
Because, if such a path exists, BFS will visit the _goal_ node, we can simply stop the algorithm
when the _goal_ node is visited and compute the path from _start_ to _goal_.

:pencil: Copy-paste your previous `bfs_traversal` function and call the new one `bfs`. Change its
signature to accept a _goal_ node and return the path between `start` and `goal`. If there is no
path between the two nodes, your function should return `None`.

```python
def bfs(graph: Dict[Node, List[Node]], start: Node, goal: Node) -> List[Node] | None:
    return None
```

:gear: You can use `pytest -k "bfs and not traversal"` to run the test.

:gear: You should now be able to run `pytest -k test_bfs` and see green.

### DFS [[TOC](#table-of-content)]

**D**epth-**F**irst **S**earch (**DFS**) is another fundamental graph traversal algorithm used to explore nodes and edges of a graph.
It's called "_depth-first_" because it aims to explore as far (deep) as possible along each branch before backtracking.

#### Question 6 [[TOC](#table-of-content)]

:pencil: Implement the DFS algorithm on a graph.

The `graph` parameter is a dictionary where the keys are the nodes and the values are lists of adjacent nodes.
The `start` parameter is the node where to start the traversal.
The `fn` parameter is a _callback_ function that takes a node and performs any operation, e.g. this function could be a simple `print`.
The function should apply the `fn` parameter to each node visited during the traversal.

```python
def dfs_traversal(graph: Dict[Node, List[Node]], start: Node, fn: Callable[[Node], Any]):
    ...
```

:gear: You can use `pytest -k "dfs and traversal"` to run the test.

#### Question 7 [[TOC](#table-of-content)]

:pencil: Same question as with BFS, write a function that computes the path between two nodes in a
graph using the DFS algorithm.

```python
def dfs(graph: Dict[Node, List[Node]], start: Node, goal: Node) -> List[Node] | None:
    return None
```

:gear: You can use `pytest -k "dfs and not traversal"` to run the test.

:gear: You should now be able to run `pytest -k test_dfs` and see green.

## Weighted Graphs [[TOC](#table-of-content)]

A weighted graph is a graph in which a number (weight) is assigned to each edge.

For instance, if our graph represents a map of roads that you can drive, two nodes will be connected
_if and only if_ there is a road between them and the _weight_ associated to that edge in the graph
could be something like the time to travel, the cost in gas or the distance.

### Weighted Graph Representation [[TOC](#table-of-content)]

> :bulb: **Note**
>
> In the following, we will be using the _adjacency list_ representation as it is easier to use.

Let's consider the weighted graph below:

```mermaid
flowchart LR
    A -- 1 --- B
    A -- 2 --- C
    B -- 3 --- D
    B -- 4 --- E
    C -- 5 --- F
    E -- 6 --- F
```

Here is its _adjacency list_ representation, written in Python with a dictionary:
```python
my_weighted_graph = {
    'A': {'B': 1, 'C': 2},
    'B': {'A': 1, 'D': 3, 'E': 4},
    'C': {'A': 2, 'F': 5},
    'D': {'B': 3},
    'E': {'B': 4, 'F': 6},
    'F': {'C': 5, 'E': 6}
}
```

### Weighted Graph Operations [[TOC](#table-of-content)]

In this type of graphs, rather than just calculating a possible path between 2 nodes, we could be interested in calculating the shortest path between two nodes. 

#### Dijkstra's Algorithm [[TOC](#table-of-content)]

Dijkstra's algorithm is an algorithm for finding the shortest paths between a starting node in a
graph and all the other nodes. In our case, we only need to find the shortest path between two nodes.
So the function we will write takes a graph, a starting node, and an ending node and returns the shortest path between the two nodes.

Here is a step-by-step description of the algorithm:
1. Initialize the distances:
    - create a set of nodes called `unvisited` that contains all the nodes in the graph
    - set the distance from the starting node to itself to 0 and all other nodes to infinity
    - set the `predecessor`of each node (to keep track of the path) to `None`
2. Select the Node with the smallest distance from the starting node:
    - from the `unvisited` set, select the node with the smallest distance from the starting node. 
    - if the selected node is the ending node, return the path from the `predecessor` dictionary.
3. Update the distances of the neighbors:
    - for each neighbor of the selected node, calculate the distance from the starting node to the neighbor through the selected node
    - if the calculated distance is smaller than the current distance, update the distance and the predecessor of the neighbor
    - Example: if the current node is A, and it has a neighbor B, the tentative distance to B is distance(A) + edge weight(A, B). If this distance is smaller than the current known distance to B, update it.
4. Mark the current node as Visited:
    - remove the current node from the `unvisited` set.
    - Once all neighbors have been visited, mark the current node as visited. This means that we found the shortest path to this node, and it will not be revisited.
5. Repeat
    - Repeat the process until the `unvisited` set is empty or the ending node is reached.
6. Return the path:
    - Once the ending node is reached, reconstruct the path from the `predecessor` dictionary and return it.

```js
1 function Dijkstra(graph, start, end):
2     unvisited ← graph.Nodes() // all nodes are initially unvisited (use a set)
3     for each node n in graph:
4         dist[n] ← INFINITY   // set the distances from the starting node to all other nodes to infinity (use float('inf'))
5         prev[n] ← UNDEFINED  // set the previous node to undefined (use None)
6     dist[start] ← 0             // the distance from the starting node to itself is 0
7
8     while unvisited is not empty:
9         u ← node in unvisited with the smallest dist[u] // use select_min function
10        if u == end:
11          return rebuild_path(prev, end)
13       for each neighbor v of u:
14           w = graph[u][v] // the weight of the edge between u and v
14           alt ← dist[u] + w
15           if alt < dist[v]:
16               dist[v] ← alt
17               prev[v] ← u
18       unvisited.remove(u)
19    return None
```

Before starting, let's define `rebuild_path` as a helper function that reconstructs the path from the `prev` dictionary and the `end` node:
```python
def rebuild_path(prev, end):
  path = []
  while end is not None:
    path.append(end)
    end = prev[end]
  return path[::-1]
```

Now, let's implement `select_min` function that returns the node with the smallest distance (implicitly from the starting node). It takes as argument:
- `dist`: the dictionary that contains the distances from the starting node to all other nodes
- `unvisited`: the set of nodes that are not visited yet

```python
def select_min(dist: Dict[Node, float], unvisited: set[Node]) -> Node:
    # write your code
```

Now, you're good to implement the `dijkstra` function.

##### Question 8 [[TOC](#table-of-content)]

:pencil: Implement the _Dijkstra_ algorithm in `graph.py`.

The function should return the shortest path between the `start` and `goal` nodes. If there is no such path, your function should return `None`.
The `graph` parameter is a dictionary where the keys are the nodes and the values are dictionaries with the adjacent nodes and their weights.
The `start` parameter is the node where the path should start.
The `goal` parameter is the node at the end of the path.

```python
def dijkstra(graph: Dict[Node, Dict[Node, float]], start: Node, goal: Node) -> List[Node] | None:
    return None
```

:gear: You should now be able to run `pytest -k test_dijkstra` and see green.

##### Question 9 [[TOC](#table-of-content)]

Let's first measure the time it takes to compute the shortest path between two nodes in a graph that looks like a chain of nodes. You will be using the `chain_graph` function below:

```python
def chain_graph(size: int) -> Dict[str, Dict[str, int]]:
    graph = {}
    for i in range(size - 1):
        graph[str(i)] = {str(i + 1): 1}
    graph[str(size - 1)] = {}
    return graph
```

:pencil: Create a _chain_ graph of $10^5$ vertices by using the `chain_graph` function above.

Below is an example of how you can measure the time it takes to compute something, in seconds:
```python
import time

start = time.time()
# something that takes some time
end = time.time()
print(end - start)
```

:pencil: Measure the time it takes to compute the shortest path between the nodes $'0'$ and $'1'$ in the
long _chain_ graph defined previously.

Now, let's optimize the algorithm to make it faster. The problem with the current implementation is that it initializes the `dist` and `prev` dictionaries with all the nodes in the graph at the start. 
However, the algorithm could never visit all these nodes, depending on the graph structure.

:pencil: Create a new function `dijkstra_with_priority_queue` to generate `dist` and `prev` "_on the fly_".

To do that, you will need to use a priority queue to keep track of the nodes to visit. You can use the `heapq` module from the Python standard library to create a priority queue.

```python
import heapq

queue = []
# add items to the priority queue
priority = 10
item = 'A'
heapq.heappush(queue, (priority, item))

priority = 8
item= 'B'
heapq.heappush(queue, (priority, item))

# get the smallest item
priority, item = heapq.heappop(queue)

assert item == 'B'
```

Now, measure the time needed to compute the shortest path between the nodes $'0'$ and $'2'$ in the $10^6$-node _chain_ graph using the new implementation.

You should see a significant improvement in the time it takes to compute the shortest path.

##### Question 10 [[TOC](#table-of-content)]

The issue with the previous implementation of the algorithm is that the `dist` and `prev` data
structures are initialized with all the nodes in the graph at the start.

So, if the graph is very large, it takes a long time to initialize them, even though the algorithm
will never visit all these nodes as previously seen.

:pencil: Create another function `dijkstra_on_the_fly` to use a priority queue as previously, and generate `dist` and `prev` "_on the fly_".

> :bulb: **Note**
>
> You can use the [`get` function](https://docs.python.org/3/library/stdtypes.html#dict.get) to check if a key is in a dictionary.

:gear: Now, measure and compare the time it takes to compute the shortest path between the nodes $'0'$ and $'2'$ in the $10^6$-node _chain_ graph.

#### A* Algorithm [[TOC](#table-of-content)]

Dijkstra's algorithm works pretty great in general and is quite simple to implement.

However, one could say that it is _blind_ in the sense that it has no idea where to go next apart
from picking the node in $Q$ that has the smallest distance from the starting point.

It would be nice if we could somehow add some information about the _distance_ to the _goal_ node!

A classic algorithm that does that is called A*. It is an extension of Dijkstra where a node will
be picked based on its current _true_ distance to the starting node AND an _estimated_ distance to
the goal node.

This _estimated_ distance is called a _heuristic_ and will help _guide_ the search towards the goal
state.

There are plenty of resources and videos that explain what this _guiding_ looks like but, if we
imagine a 2D map with the _euclidean_ distance as the heuristic, A* will try to aim directly at the
goal first because nodes in the direction of the goal will have a much smalled heuristic value and
thus will be picked first!

> :bulb: **Note**
>
> Dijkstra and A* are closely related. One could either say that
> - A* is a generalization of Dijkstra where a heuristic is added
> - Dijkstra is a special case of A* where the heuristic is the constant $0$

##### Formalizing the algorithm [[TOC](#table-of-content)]
Let's call the starting point $s$ and the goal node $e$ (as in _end_).

Instead of keeping track of the distance from $s$ alone, let's define three quantities for a given node $n$:
- $g(n)$ will be the _true_ distance from $s$ to $n$, i.e. the Dijkstra's _distance_
- $h(n)$ will be the _estimated_ distance from $n$ to $e$, given by the heuristic
- $f(n)$ will be the total score of node $n$, i.e. $f(n) = g(n) + h(n)$

First, A* will initialize a structure that keeps track of the $g$ values for all nodes $n$ seen so
far and another one tracking the $f$ values.

Then, a node will be picked based on its $f$ value, i.e. $Q$ will be sorted according to the $f$ values.

Finally, during the neighbor update, a new $g$ score is computed. For a neighbor $n'$, the score is
$$g'(n') = g(n) + w(n, n')$$ where $w(a, b)$ is the weight of the edge between $a$ and $b$.
If the new $g$ score of $n'$ is smaller than its previous value, i.e. $g'(n') \lt g(n')$, then
- $g(n') \leftarrow g'(n')$
- $f(n') \leftarrow g'(n') + h(n')$
- $n'$ is added to $Q$ if it is not already in there

Apart from these new $f$ values, the algorithm is the same.

##### Question 11 [[TOC](#table-of-content)]

:pencil: Implement the _A*_ algorithm.

It should take a `graph`, `start` and `goal` as before as well as a new `h` argument, the heuristic.

`h` should take two nodes as parameters and return a number, the _estimated_ distance between the
two nodes.

> :bulb: **Note**
>
> in the `a_star` function, you will be calling `h` as `h(n, goal)`.

```python
def a_star(
    graph: Dict[Node, Dict[Node, float]],
    start: Node,
    goal: Node,
    h: Callable[[Node, Node], float],
) -> List[Node] | None:
    return None
```

:gear: You should now be able to run `pytest -k test_a_star` and see green.

## Very large or infinite graphs [[TOC](#table-of-content)]
### A monster is lurking... [[TOC](#table-of-content)]
Let's take an example where our current graph representations will hit their limits.

Instead of a nice, simple and small graph, we will play a game. You can move your player around a 2D
world which extends infinitely in all directions.

However, this world is not all that peacefull and a monster is lurking around somewhere, looking for
your... :smirk:

This monster will use _shortest path_ algorithms such as Dijkstra or A* to find the best path
towards you!

The issue is that the graph of the world is infinite and thus, it cannot be represented as a Python
dictionary :confused:
But the thing is, even though the graph might be infinite, it is well defined everywhere and,
because the path from the monster to you is always finite, it must be possible to solve this issue!

### Generating a graph _on the fly_ [[TOC](#table-of-content)]
Up until know, we have been "_collecting_" all the information about our graphs into a dictionary
before the algorithms start.

The good news is that it is possible to do this on the fly, as the _shortest path_ algorithms run.

In order to achieve that, we will be representing any graph by a function. This function will take
a node as its single input and return the list of all its neighbors, with the weight associated to
each node connection.

With that, our _shortest path_ algorithms will be able to _query_ the graph at each node to get its
neighbors and their associated weights. If you look closely to your algorithms, this should be the
only time you need to access the graph!

Let's take the same _weighted_ graph as before. As a reminder, it looks like this
```mermaid
flowchart LR
    A -- 1 --- B
    A -- 2 --- C
    B -- 3 --- D
    B -- 4 --- E
    C -- 5 --- F
    E -- 6 --- F
```

And its _adjacency list_ representation is as follows:
```python
graph = {
    'A': {'B': 1, 'C': 2},
    'B': {'A': 1, 'D': 3, 'E': 4},
    'C': {'A': 2, 'F': 5},
    'D': {'B': 3},
    'E': {'B': 4, 'F': 6},
    'F': {'C': 5, 'E': 6}
}
```

### Question 12 [[TOC](#table-of-content)]
:pencil: Define a function `graph` that computes the list of neighbors and their weights.

```python
def graph(node: Node) -> List[Tuple[Node, float]]:
    return []
```

### Question 13 [[TOC](#table-of-content)]
:file_folder: Create a new file called `src/graph_inf.py`.

:pencil: Rewrite the `dijkstra` and `a_star` function to accept a function in place of the _graph_.

They should now have the following signatures:

```python
def dijkstra(
    graph: Callable[[Node], List[Tuple[Node, float]]],
    start: Node,
    goal: Node,
) -> List[Node] | None:
    return None
```
```python
def a_star(
    graph: Callable[[Node], List[Tuple[Node, float]]],
    start: Node,
    goal: Node,
    h: Callable[[Node, Node], float],
) -> List[Node] | None:
    return None
```

:gear: Running the following command should be green if you have correctly made the transition.
```shell
pytest -k graph_inf
```

---
---
> [go to next](project.md)
