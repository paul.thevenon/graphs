# The Eight puzzle problem [[TOC](README.md#the-8-puzzle)]

## The problem [[TOC](README.md#the-8-puzzle)]

The eight puzzle problem is a puzzle invented and popularized by Noyes Palmer Chapman in the 1870.
It is played on a 3-by-3 grid with 8 square tiles labeled 1 through 8 and a blank square.
Your goal is to rearrange the blocks so that they are in order.
You can only move the blank square up, down, left, or right. You can't move a tile diagonally.

Similar names are used for the problem, such as the 8-puzzle, 15-puzzle, and 24-puzzle, depending on the size of the grid.

### Question 0 [[TOC](README.md#the-8-puzzle)]

Let's start by creating a class to represent the puzzle.

:file_folder: Create a file named `puzzle.py`.

:pencil: Add the following code to `puzzle.py`.
```python
from typing import List, Tuple


class Puzzle:
    def __init__(self, board: List[List[int]]):
        self.board = board

    def __eq__(self, other) -> bool:
        return self.board == other.board

    def __repr__(self) -> str:
      return str(self.board)
```

The `Puzzle` class has an `__init__` method that receives a board as a list of lists, a `__eq__` method to compare two puzzles and a `__repr__` method to print the board.

:pencil: Create a puzzle instance and print the board.

As convention, we will represent the blank square with -1.
A puzzle is a list of lists, where each list represents a row of the board.

## Displaying the board [[TOC](README.md#the-8-puzzle)]

Now, let's display the board in a more readable way.

To do that, we will use the [`src.puzzle`](./src/puzzle.py) module.

### Question 1 [[TOC](README.md#the-8-puzzle)]

:mag: Read the [doc of `src.puzzle`](src/README.md#using-the-srcpuzzle-library).

:pencil: Make sure the example provided in the documentation works fine for you.

> :bulb: **Note
>
> you should see a cowboy in a 3x3 grid with a blank bottom right square.

## Shuffling the board [[TOC](README.md#the-8-puzzle)]

We cannot randomly generate a board because it may not be solvable. We need to start with a solved board and shuffle it.
To shuffle the board, we will create a method called `shuffle_board` that will create a board, and randomly move the blank square up, down, left, or right.

### Question 2 [[TOC](README.md#the-8-puzzle)]

:pencil: Create a function called `shuffle_board`

```python
def shuffle_board(nb_rows, nb_cols, nb_moves=100) -> List[List[int]]:
    board = [[0, 1, 2], [3, 4, 5], [6, 7, -1]] # use nb_rows and nb_cols to build a board with the correct dimensions
    return board
```

> :bulb: **Hint**
>
>
> 1. First, we need to find the position of the blank square (the `-1` cell). Create a variable `zero_pos` as a tuple with the row and column of the blank square.
> 2. Now, write a loop that will move the blank square 100 times. In each iteration, randomly choose a move (up, down, left, or right) and move the blank square.

:gear: Once your function is ready, you can check your code with the following test:

```bash
pytest -k "test_shuffling"
```

### Displaying the shuffled board [[TOC](README.md#the-8-puzzle)]

#### Question 3 [[TOC](README.md#the-8-puzzle)]

:pencil: Display the shuffled board using the previous code.

## Solving the puzzle [[TOC](README.md#the-8-puzzle)]

To solve the puzzle, we will use and compare different search algorithms we wrote in previous exercises.

So, our nodes will be the board states (an instance of `Puzzle`), and the edges will be the possible moves.

As we wrote in the previous exercises, we need a special function `get_neighbors` that will return a list of tuples containing the next possible Puzzles associated with a weight.
In your case, the weight is always `1` (as a `float`), as it cost the same to move the blank square in any direction.

Before that, we need to modify the `Puzzle` class to add a `move` attribute to keep track of the move that generated the current state.
Indeed, we need to know the move to validate the path from the initial state to the goal state.

This attribute must be `None` by default.

### Question 4 [[TOC](README.md#the-8-puzzle)]

:pencil: Modify the `Puzzle` class to include the `move` attributes.
- First, add the attributes to the `__init__` method. By default, the `move` attribute is `None`.
- Initialize the attribute in the `__init__` method.

### Hashing a `Puzzle` [[TOC](README.md#the-8-puzzle)]

As you know, we need to _hash_ the `Puzzle` instances to use them as keys in a dictionary.
To do that, we will implement the `__hash__` method in the `Puzzle` class.
This function will return a hash of the board. To perform hash, you use the builtin [`hash`](https://docs.python.org/3/library/functions.html#hash) function.

#### Question 5 [[TOC](README.md#the-8-puzzle)]

:pencil: Implement the `__hash__` method for the `Puzzle` class
```python
def __hash__(self):
    # find a way to hash the board and return it
```

### The possible moves [[TOC](README.md#the-8-puzzle)]

From a given state (a `Puzzle`), we can move the blank square up, down, left, or right.
These moves represent the edges of our graph that are the neighbors of a node.

#### Question 6 [[TOC](README.md#the-8-puzzle)]

:pencil: Create a function called `get_neighbors` that will return a list of possible moves (instances of Puzzle).

```python
def get_neighbors(puzzle: Puzzle) -> List[Tuple[Puzzle, float]]:
    return []
```

:gear: You can test the method by creating a puzzle instance and printing the neighbors.

```bash
pytest -k "neighbors"
```

Now, you have the initial state (the `Puzzle` to solve) and the possible moves from the initial state.
We can use the search algorithms we wrote in previous exercises to solve the puzzle.

Then, we will need a heuristic function. Of course, in this case, we will not use the Euclidean distance.

## Plugging the search algorithms [[TOC](README.md#the-8-puzzle)]

Now, we will apply the search algorithms we wrote in previous exercises to solve the puzzle.

### Question 7 [[TOC](README.md#the-8-puzzle)]

:pencil: Create a `Puzzle` instance with a shuffled board.

:pencil: Create the `Puzzle` solution with the solved board.

:pencil: Apply the following search algorithms to solve the puzzle:
- Breadth-first search
- Depth-first search
- Dijkstra

:question: Compare the results and the time it took to solve the puzzle.

### Validate the path [[TOC](README.md#the-8-puzzle)]

To validate the path from the initial state to the goal state, we need to check if the path is valid.

#### Question 8 [[TOC](README.md#the-8-puzzle)]
:pencil: Create a function called `is_valid_path` that will take a path as a list of `Puzzle` instances and return `True` if the path is valid, `False` otherwise.

```python
def is_valid_path(path: List[Puzzle], start: Puzzle, goal: Puzzle) -> bool:
    return False
```

Make sure that the first state is the initial state, the last state is the goal state, and the path is valid.

:gear: You can test the method by creating a path and checking if it is valid.

```bash
pytest -k "is_valid_path"
```

### Displaying the path [[TOC](README.md#the-8-puzzle)]

Finally, we will display the path of the puzzle.

#### Question 9 [[TOC](README.md#the-8-puzzle)]
:pencil: Write the code that displays the path of the puzzle using the `src.puzzle` module. You can use the method `show_solution` to display the path as shown in the example.
This function takes a list of _boards_ (not `Puzzle`) as an argument. You can access the board of a `Puzzle` instance using the `board` attribute.

> :bulb: **Note**
>
> Of course, if the path is too long (e.g., more than 100 moves), don't display it.

## A* Search and the distance [[TOC](README.md#the-8-puzzle)]

Now, let's try to solve the puzzle using the A* search algorithm. You noticed that we need a heuristic function to use the A* search algorithm.

In the previous exercises, we used the Euclidean distance as a heuristic function. Indeed, it was easy to calculate the distance between two points in a 2D plane.
However, in this case, there's no meaningful concept of distance between states beyond the number of moves required to transition from one to another.

### The Manhattan distance [[TOC](README.md#the-8-puzzle)]

In the context of the 8-puzzle, we need a heuristic function that estimates the number of moves required to reach the goal state from a given state.

To compute the Manhattan distance from a state to the goal state, we need to calculate the sum of the distances of each tile from its goal position in the grid.
This sum gives us an estimate of how far the state is from the goal state.

For example, in the following state:

```
[[1, 2, 3],
 [4, 5, 6],
 [7, 8, -1]]
```

The Manhattan distance is `0` because all the tiles are in their goal positions.

In the following state:

```
[[1, 2, 3],
 [4, 5, 6],
 [7, -1, 8]]
```

The Manhattan distance is `1` because the tile `8` is on the correct row but one column to the right.

#### Question 10 [[TOC](README.md#the-8-puzzle)]

:pencil: Create a function called `manhattan_distance` taking a state (a `Puzzle`) that will return the Manhattan distance from the current state to the goal state.

```python
def manhattan_distance(state: Puzzle, goal_state: Puzzle) -> float:
    return 0.
```

:gear: You can test the method by creating a puzzle instance and calculating the Manhattan distance to the goal state.

```bash
pytest -k "manhattan_distance"
```

### Applying the A* search algorithm [[TOC](README.md#the-8-puzzle)]

#### Question 11 [[TOC](README.md#the-8-puzzle)]
:pencil: Now, we can apply the A* search algorithm to solve the puzzle using the Manhattan distance as a heuristic function.

:question: Compare the results and the time it took to solve the puzzle.

### Displaying the solution [[TOC](README.md#the-8-puzzle)]

#### Question 12 [[TOC](README.md#the-8-puzzle)]
:pencil: Finally, display the solution of the puzzle using the `src.puzzle` module.

## Generalizing the puzzle [[TOC](README.md#the-8-puzzle)]

Now, let's generalize the puzzle to any size of the grid.

### Question 13 [[TOC](README.md#the-8-puzzle)]
:pencil: Complete the following steps:
- Modify the `Puzzle` class to accept any size of the grid, and keep track of the size of the grid (the number of rows and columns).
- Modify the `manhattan_distance` method to work with any size of the grid.
- Modify the `get_neighbors` method to work with any size of the grid.
- Modify the `shuffle_board` method to work with any size of the grid.
- Modify the `is_valid_path` method to work with any size of the grid.

Once you have modified your code, you can still use the displaying methods to display the board and the solution.
They just need a `board` as a list of lists containing the values of the tiles.

:question: What happens if you try with bigger grids? 3x4? 4x4?

---
---
> this is the end of the class :clap: :clap:
>
> [return to TOC](README.md#table-of-content)
