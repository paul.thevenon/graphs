def test_graph_count():
    graph = {
        '1': ['2', '3', '6'],
        '2': ['1', '3', '4'],
        '3': ['1', '2', '4', '6'],
        '4': ['2', '3', '5'],
        '5': ['4', '6'],
        '6': ['1', '3', '5']
    }

    from src.graph import count_nodes_edges
    assert count_nodes_edges(graph) == (6, 9), "incorrect number of nodes and edges"


def test_graph_degree():
    graph = {
        '1': ['2', '3', '6'],
        '2': ['1', '3', '4'],
        '3': ['1', '2', '4', '6'],
        '4': ['2', '3', '5'],
        '5': ['4', '6'],
        '6': ['1', '3', '5']
    }

    from src.graph import degree
    for n, d in [('1', 3), ('2', 3), ('3', 4), ('4', 3), ('5', 2), ('6', 3)]:
        actual = degree(graph, n)
        assert actual == d, f"degree of node {n} should be {d}, found {actual}"


def test_graph_neighbors():
    graph = {
        '1': ['2', '3', '6'],
        '2': ['1', '3', '4'],
        '3': ['1', '2', '4', '6'],
        '4': ['2', '3', '5'],
        '5': ['4', '6'],
        '6': ['1', '3', '5']
    }

    from src.graph import neighbors
    for n, expected in [
        ('1', ['2', '3', '6']),
        ('2', ['1', '3', '4']),
        ('3', ['1', '2', '4', '6']),
        ('4', ['2', '3', '5']),
        ('5', ['4', '6']),
        ('6', ['1', '3', '5']),
    ]:
        actual = neighbors(graph, n)
        assert actual == expected, f"neighbors of node {n} should be {expected}, found {actual}"
