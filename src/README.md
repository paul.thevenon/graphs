## using the `src.ui` module
the goal of this library is to provide an easy-to-use graphical application. It
will take a map as an input and display it with pretty 2D tiles.


Below is an example of how that module could be used:
```python
from typing import List, Tuple, Callable
from time import time_ns
from perlin_noise import PerlinNoise

from src.ui import Canvas, create_graph_from_grid, show_grid

FRAME_RATE = 60
TILE_SIZE = 32

MAP_WIDTH, MAP_HEIGHT = 40, 20

Node = Tuple[int, int]
Graph = Callable[[Node], List[Tuple[Node, float]]]


def generate_terrain(
    width: int,
    height: int,
    noise: List[Tuple[float, PerlinNoise]],
    water_level: float,
    rock_level: float,
) -> List[List[str]]:
    return [['w' for _ in range(width)] for _ in range(height)]


def generate_biome(
    width: int,
    height: int,
    noise: List[Tuple[float, PerlinNoise]],
    forest_level: float,
) -> List[List[str]]:
    return [[' ' for _ in range(width)] for _ in range(height)]


def dijkstra(
    graph: Graph, start: Node, end: Node
) -> Tuple[List[Node], List[Node], List[Node]]:
    return [], [], []


def a_star(
    graph: Graph, start: Node, end: Node
) -> Tuple[List[Node], List[Node], List[Node]]:
    return [], [], []


if __name__ == "__main__":
    terrain = generate_terrain(
        width=MAP_WIDTH,
        height=MAP_HEIGHT,
        noise=[],
        water_level=0.0,
        rock_level=0.0,
    )
    biome = generate_biome(
        width=MAP_WIDTH,
        height=MAP_HEIGHT,
        noise=[],
        forest_level=0.0,
    )

    print("terrain")
    show_grid(terrain)
    print("biome")
    show_grid(biome)

    graph = create_graph_from_grid(terrain, biome)

    grid_size = (len(terrain), len(terrain[0]))

    canvas = Canvas(
        grid_size, tile_size=TILE_SIZE, frame_rate=FRAME_RATE, seed=time_ns()
    )
    canvas.setup(dijkstra, terrain, biome, graph)

    canvas.loop()
```

### keybindings and mouse
- you can quit the application by pression _Escape_
- you can click the left mouse button to select a first node, i.e. the start node of the
  _shortest path_ algorithms
- you can move the mouse around to select the end node wherever the mouse is
- you can click the right mouse button to unselect the node

### rendering conventions
- red circles: the path from start to end
- green circles: all the nodes that have been visited
- blue circles: the frontier of next nodes to visit

## using the `src.puzzle` library
### an example
```python
from src.puzzle import Canvas
import os

IMAGE_PATH = "./assets/ai-cartoon-cowboy.jpg"
BOARD_SIZE = (3, 3)

canvas = Canvas(frame_rate=15)
n, m = BOARD_SIZE
canvas.setup(os.path.expanduser(IMAGE_PATH), n=n, m=m, scaling_factor=0.5)

canvas.board = ... # e.g. you can set the board to a randomly shuffled one

canvas.loop()
```

### color conventions
when something is happening, there will be a colored rectangle at the border of the canvas:
- red: the board is being shuffled
- blue: an algorithm is thinking...
- green: the solution found is being replayed
